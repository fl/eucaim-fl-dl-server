#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import yaml
import torch
import warnings

import flwr as fl
import numpy as np

from typing import List
from pathlib import Path

# Define output directories
experiment_dir = Path( 'results' )
history_dir = experiment_dir / 'history'

class SaveModelStrategy( fl.server.strategy.FedAvg ):
    def aggregate_fit( self, server_round, results, failures ):
        aggregated_parameters, aggregated_metrics = super().aggregate_fit( server_round, results, failures )

        if aggregated_parameters is not None:
            # Convert `Parameters` to `List[np.ndarray]`
            aggregated_ndarrays: List[np.ndarray] = fl.common.parameters_to_ndarrays( aggregated_parameters )

            # Save aggregated_ndarrays
            print( f"Saving round {server_round} aggregated_ndarrays ( {experiment_dir}/round-{server_round}-weights.npz ..." )
            np.savez( experiment_dir / f"round-{server_round}-weights.npz", *aggregated_ndarrays )
            print( '... done' )

        return aggregated_parameters, aggregated_metrics


if __name__ == '__main__':
    # Create them
    experiment_dir.mkdir( parents = True, exist_ok = True )
    history_dir.mkdir( parents = True, exist_ok = True )


    # Start Flower server for three rounds of federated learning
    if os.getenv( 'NOSECURE' ) is not None:
        history = fl.server.start_server(
            server_address = 'localhost:4433', 
            config         = fl.server.ServerConfig( num_rounds = 3 ), 
            strategy       = SaveModelStrategy(
                min_fit_clients       = 3,
                min_evaluate_clients  = 3,
                min_available_clients = 3
            ),
            certificates   = None
        )
    else:
        history = fl.server.start_server(
            server_address = '0.0.0.0:4433', 
            config         = fl.server.ServerConfig( num_rounds = 3 ), 
            strategy       = SaveModelStrategy(
                min_fit_clients       = 3,
                min_evaluate_clients  = 3,
                min_available_clients = 3
            ),
            certificates   = (
                Path( '.cache/certificates/rootCA_cert.pem' ).read_bytes(),
                Path( '.cache/certificates/server_cert.pem' ).read_bytes(),
                Path( '.cache/certificates/server_key.pem' ).read_bytes(),
            )
        )
    
    # Save the history as a yaml file
    with open( history_dir / 'history.yaml', 'w' ) as fr:
        yaml.dump( history, fr )
