# EUCAIM FL DL Server


## Introduction

This repository contains the `Dockerfile` and the `server.py` for the server to be used in the [EUCAIM-WP6's demonstration](https://github.com/EUCAIM/fl_demonstrator) for Federated Learning using deep learning models (aka. CNN).

## To be run

This server is to be run with its partner client located [here](https://gitlab.bsc.es/fl/eucaim-fl-dl-client). The orchestration of the experiment is to be done using the [FLmanager](https://gitlab.bsc.es/fl/FLManager).

## Installing from the image

FEM-client assumes that the image of the tools are pre-installed in the host's machine. Follow this instructions to install the image of eucaim-fl-dl-server:

1. Download the image from the docker registry. The credentials to log in are the same username and password used to access GitLab.

```
docker login gitlab.bsc.es
docker pull registry.gitlab.bsc.es/fl/eucaim-fl-dl-server:v2.0
docker logout #optional, if you don't want to keep logged in in this machine
```

2. Once the download is finished, check that docker can find ir with the following command:
```
docker images
```
